#include <fstream>
#include "labels.h"


void fetch_project_labels(const char* token, const char* project_id, labels_container& labels, bool verbose)
{
    static const std::string gitlab_project_url("https://gitlab.com/api/v4/projects/");
    curl_session sess(false);
    sess.append_header(("Authorization: Bearer " + std::string(token)));
    auto code = sess.request((gitlab_project_url + project_id) + "/labels/");
    if (code)
    {
        fprintf(stderr, "[labels]: curl request error: %s", curl_easy_strerror(code));
        return;
    }
    json::value data(json::parse(sess.response_body));
    sess.response_body.clear();
    if (data.is_array())
    {
        try
        {
            std::lock_guard<std::mutex> lock(mut);

            for (auto& obj : data.as_array())
            {
                if (!obj.is_object())
                    cerr << "[labels]: Not object:\t" << obj << '\n';
                else
                    labels.emplace_back(std::move(obj.as_object())["name"].as_string());
            }
        }
        catch (std::invalid_argument& e)
        {
            fprintf(stderr, "[labels]: invalid argument: %s\n", e.what());
        }
        if (verbose)
        {
            cout << "labels: { ";
            for (auto& i : labels)
            {
                cout << i << ", ";
            }
            cout << "}\n";
        }
    }
    else
    {
        cerr << "unexpected data kind while fetching labels:\t" << data.kind() << '\n';
    }
}