#ifndef CURL_SESSION_H
#define CURL_SESSION_H

#include <memory>

#include <iostream>
#include <string>

#include <cstring>
#include <curl/curl.h>

size_t write_memory(void* contents, size_t size, size_t nmemb, void* userdata);

struct curl_session
{
    inline explicit curl_session(bool verbose):
        res(CURLcode::CURLE_OK), curl(curl_easy_init()), verbose(verbose)
    {
        res = curl_global_init(CURL_GLOBAL_SSL);
        if (res != CURLE_OK)
        {
            fprintf(stderr, "curl code: %d", res);
            throw std::runtime_error("error initializing curl.");
        }
    };

    void append_header(const std::string& header)
    {
        header_list = curl_slist_append(header_list, header.c_str());
    };

    CURLcode append_body(const char* body, bool post = true);

    CURLcode request(const std::string& url);

    inline void cleanup_headers()
    {
        if (header_list)
            curl_slist_free_all(header_list);
        header_list = nullptr;
    }

    inline ~curl_session()
    {
        curl_easy_reset(curl);
        curl_easy_cleanup(curl);
        cleanup_headers();
        curl_global_cleanup();
    }

    CURLcode res;
    CURL* curl = nullptr;
    struct curl_slist* header_list = nullptr;
    std::string response_body;
    bool verbose;
};

#endif //CURL_SESSION_H
