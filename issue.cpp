
#include "issue.h"


void print_field(std::ostream& os, uint wid, const char* c)
{
    auto len = strlen(c);
    os << ' ';
    for (int i = 0; i < wid; ++i)
    {
        if (i < len)
        {
            os << c[i];
        }
        else
        {
            os << ' ';
        }
    }
    os << " |";
}

std::ostream& operator<<(std::ostream& os, const issue& iss)
{
    os << "| " << std::setw(issue::widths[0].second) << iss.iid() << " |";

    print_field(os, issue::widths[1].second, iss.title());

    os << ' ' << std::setw(issue::widths[2].second) << iss.state() << " |";

    std::ostringstream oss{};
    if (!iss.labels().empty())
        std::for_each(iss.labels().cbegin(), iss.labels().cend(), [&](const boost::json::value& label) {
            std::string l(label.as_string());
            if (l.front() == '\"')
            {
                l.erase(0);
                l.erase(l.end() - 1);
            }
            oss << ' ' << l;
        });
    print_field(os, issue::widths[3].second, oss.str().c_str());
    os << '\n';
    if (iss.print_desc && strlen(iss.desc()))
    {
        os << "| Desc: " << std::setw(118) << iss.desc() << " |\n";
    }
    return os;
}

void issue::print_header(std::ostream& os)
{
    os << "| " << std::setw(issue::widths[0].second) << issue::widths[0].first
       << " |";
    for (int i = 1; i < std::size(issue::widths); ++i)
    {
        os << ' ' << std::setw(issue::widths[i].second) << issue::widths[i].first << " |";
    }
    os << '\n';
}

void issue::print_break(std::ostream& os)
{
    os << std::setw(128) << std::setfill('-') << '-' << '\n';
    os.fill(' ');
}

bool issue::is_issue(boost::json::object& obj)
{
    return std::all_of(std::begin(keys), std::end(keys), [&](const char* k) {
        return obj.contains(k);
    });
}

void issue::save(curl_session& sess)
{
    auto ser = json::serialize(m_obj);
    auto code = sess.append_body(ser.c_str());
    /* https://docs.gitlab.com/ee/api/issues.html#new-issue */
    if (code)
    {
        fprintf(stderr, "[issues]: curl append request body error: %s\n", curl_easy_strerror(code));
        return;
    }
    code = sess.request(issues_endpoint());
    if (code)
    {
        fprintf(stderr, "[issues]: curl request error: %s\n", curl_easy_strerror(code));
        return;
    }
    if (sess.res == CURLE_OK)
    {
        json::value data(json::parse(sess.response_body));
        sess.response_body.clear();
        if (data.is_object())
        {
            if (data.as_object().contains("error"))
            {
                char error_msg[50];
                sprintf(error_msg, "gitlab error: %s", data.as_object()["error"].get_string().c_str());
                throw std::runtime_error(error_msg);
            }
            else
            {
                json::object& obj = data.as_object();
                auto& iid = obj["iid"].as_int64();
                if (iid)
                    m_obj["iid"] = iid;
                auto& state = obj["state"].as_string();
                if (!state.empty())
                    m_obj["state"] = state;
            }
        }
        issue::print_issue(cout, *this);
    }
    else
    {
        cout << "code:\t" << code << '\n';
    }
}

bool issue::create(curl_session& sess, labels_container& labels, std::thread& t)
{

    issue iss;
    std::string in;
    printf("Title: \n> ");
    std::getline(cin, in);
    iss.set_title(in);
    in.clear();
    printf("Desc: \n> ");
    std::getline(cin, in);
    iss.set_desc(in);
    in.clear();
    info("Labels: [empty line to finish]");
    t.join();
    info(labels);
    printf("> ");

    while (std::getline(cin, in))
    {
        if (in.empty())
            break;
        iss.push_label(in);
        in.clear();
    }
    try
    {
        iss.save(sess);
        return true;
    }
    catch (std::runtime_error& e)
    {
        fprintf(stderr, "[issues]: issue create() runtime error: %s\n", e.what());
        return false;
    }
}

bool issue::list(curl_session& sess, const std::string& filters)
{
    {
        std::string endpoint(issues_endpoint());
        if (!filters.empty())
            endpoint += ('?' + filters);
        auto code = sess.request(endpoint);
        if (code)
        {
            fprintf(stderr, "[issues]: curl request error: %s", curl_easy_strerror(code));
            return false;
        }
        json::value data(json::parse(sess.response_body));
        sess.response_body.clear();
        if (data.is_array())
        {
            std::vector<issue> issues;
            try
            {
                for (auto& obj : data.as_array())
                {
                    if (!obj.is_object())
                        cerr << "not object:\t" << obj << '\n';
                    else
                        issues.emplace_back(std::move(obj.as_object()));
                }
                issue::print_issues(cout, issues);
                return true;
            }
            catch (std::invalid_argument& exc)
            {
                fprintf(stderr, "[issues]: invalid argument e.what():  %s\n", exc.what());
                return false;
            }
        }
        else if (data.is_object())
        {
            json::object& obj = data.as_object();
            if (issue::is_issue(obj))
            {
                issue iss(std::move(obj));
                std::vector<issue> v{iss};
                issue::print_issues(cout, v);
                return true;
            }
            else if (obj.contains("error"))
            {
                fprintf(stderr, "[issues]: error from api: %s\n", obj.at("error").as_string().c_str());
                return false;
            }
            else if (obj.contains("message"))
            {
                fprintf(stderr, "[issues]: message from api: %s\n", obj.at("message").as_string().c_str());
                return true;
            }
            else
            {
                fprintf(stderr, "[issues]: not an issue\n");
                cerr << "data: " << obj << '\n';
                return false;
            }
        }
        else
        {
            cerr << "unexpected data kind: " << data.kind() << '\n';
            return false;
        }
    }
}

bool issue::get(curl_session& sess, const std::string& iid)
{
    auto code = sess.request(issues_endpoint() + iid + '/');
    if (code)
    {
        fprintf(stderr, "[issues]: curl request error: %s", curl_easy_strerror(code));
        return false;
    }
    json::value data(json::parse(sess.response_body));
    sess.response_body.clear();
    if (data.is_object())
    {
        print_issue(cout, issue(std::move(data.as_object()), true));
        return true;
    }
    else
    {
        cerr << "unexpected data kind: " << data.kind() << '\n'
             << "data:\n"
             << data << '\n';
    }
    return false;
}
