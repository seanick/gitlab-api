## GitLab Issues API
### Description
CLI tool to create and list GitLab project issues.


### Usage
    gitlab-api list -f labels=backend::bug
    gitlab-api create
    gitlab-api get -i 23

### Build

    $ mkdir build && cd build
    $ cmake ..
    $ make

#### Run
    $ gitlab-api --project_id 12341234 list
