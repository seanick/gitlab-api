#include "curl-session.h"

size_t write_memory(void* contents, size_t size, size_t nmemb, void* userdata)
{
    size_t real_size = size * nmemb;
    static_cast<std::string*>(userdata)->append(static_cast<char*>(contents), real_size);
    return real_size;
}

CURLcode curl_session::append_body(const char* body, bool post)
{
    append_header("Accept: application/json");
    append_header("Content-Type: application/json");
    if (post)
    {
        curl_easy_setopt(curl, CURLOPT_POST, 1L);
    }
    else
    {
        curl_easy_setopt(curl, CURLOPT_PUT, 1L);
    }
    std::cout << "append_body body:\t" << body << '\n';
    return curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
}

CURLcode curl_session::request(const std::string& url)
{
    curl_easy_setopt(curl, CURLoption::CURLOPT_URL, url.c_str());
    if (verbose)
        curl_easy_setopt(curl, CURLoption::CURLOPT_VERBOSE, 1L);

    curl_easy_setopt(curl, CURLoption::CURLOPT_HTTPHEADER, header_list);
    curl_easy_setopt(curl, CURLoption::CURLOPT_WRITEFUNCTION, write_memory);
    curl_easy_setopt(curl, CURLoption::CURLOPT_WRITEDATA, &response_body);
    try
    {
        res = curl_easy_perform(curl);
        if (res == CURLcode::CURLE_OK)
        {
            /* while this was useful in printing the size of response body,
             * this printed to stdout if the labels thread just finished its
             * request, before the user finished typing. figure out another
             * way to print this if really needed. */
            // printf("%lu bytes received\n", response_body.size());
            long lng;
            curl_easy_getinfo(curl, CURLINFO::CURLINFO_RESPONSE_CODE, &lng);
            if (verbose)
            {
                auto print_break = []() {
                    printf("# ------------------------ #\n");
                };
                print_break();
                printf("Response status_code: %li\n", lng);
                char* ct;
                double d;
                curl_easy_getinfo(curl, CURLINFO::CURLINFO_CONTENT_TYPE, &ct);
                printf("Response content-type: %s\n", ct);
                curl_easy_getinfo(curl, CURLINFO::CURLINFO_TOTAL_TIME, &lng);
                printf("Response total time: %lu\n", lng);
                curl_easy_getinfo(curl, CURLINFO::CURLINFO_SPEED_DOWNLOAD, &d);
                printf("Response download speed: %8f\n", d);
                curl_easy_getinfo(curl, CURLINFO::CURLINFO_REQUEST_SIZE, &lng);
                printf("Response request size: %lu\n", lng);
                printf("Response data:\n%s\n", response_body.c_str());
                print_break();
            }
        }
        else
        {
            fprintf(stderr, "error code: %d\n", res);
        }
    }
    catch (std::bad_alloc& e)
    {
        fprintf(stderr, "bad allocation: %s\n", e.what());
    }
    cleanup_headers();
    return res;
}
