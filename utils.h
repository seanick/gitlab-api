#ifndef GITLAB_API__UTILS_H
#define GITLAB_API__UTILS_H

#include <type_traits>
#include <array>

#include <mutex>
#include <thread>

template<typename T, typename = void>
struct is_iterable : std::false_type
{};

template<typename T>
struct is_iterable<T, std::void_t<decltype(std::begin(std::declval<T>())), decltype(std::end(std::declval<T>()))>> : std::true_type
{};

template<typename T>
concept Iterable = is_iterable<T>::value;

static const std::string gitlab_project_url("https://gitlab.com/api/v4/projects/");

namespace json = boost::json;

using std::cin, std::cerr, std::cout;

template<typename T>
void info(const T& t)
{
    cout << t << '\n';
}

template<typename T, typename... Args>
void info(const T& t, const Args&... args)
{
    cout << t << '\n';
    info(args...);
}

template<Iterable V>
void info(const V& v)
{
    cout << "{ ";
    for (auto& i : v)
    {
        cout << i << ", ";
    }
    cout << "}\n";
}

#endif //GITLAB_API__UTILS_H
