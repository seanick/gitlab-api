#include <iostream>
#include <string>
#include <filesystem>
#include <fstream>

#include <boost/json.hpp>
#include <boost/program_options.hpp>

/* include to build as standalone executable, to avoid linking boost::json */
#include <boost/json/src.hpp>

#include "curl-session.h"
#include "issue.h"
#include "labels.h"

namespace po = boost::program_options;

const char* project_id = nullptr;
std::mutex mut;

int main(int argc, char* argv[])
{

    auto desc_help = "\nUsage: gitlab-api token issue";
    const std::string token_var_key("token-var"),
      command_key("command"),
      help_key("help"),
      issue_key("issue-id"),
      filter_key("filter"),
      config_file_key("settings_file_key"),
      project_id_key("project_id");

    bool verbose_flag(false);
    bool help_flag(false);
    const char* default_config_file = ".config.json";

    po::options_description options(desc_help);
    // clang-format off
  options.add_options()
	  ((help_key + ",h").c_str(), po::bool_switch(&help_flag), "help")
	  ((token_var_key + ",t").c_str(), po::value<std::string>()->default_value("GIT_TOKEN"), "Token var to read from env [ default: GIT_TOKEN ]")
	  ((command_key + ",c").c_str(), po::value<std::string>()->default_value("")->required(), "COMMAND [ get | create | update | list ]")
	  ((issue_key + ",i").c_str(), po::value<std::string>()->default_value(""), "issue id [ for use only with get and update ]")
	  ((filter_key + ",f").c_str(), po::value<std::string>()->default_value(""), "filter issues [ for use only with list ]")
      ((config_file_key + ",s").c_str(), po::value<std::string>()->default_value(default_config_file), "settings json file to save \"project_id\" & will later store labels.")
      ((project_id_key + ",p").c_str(), po::value<std::string>()->default_value(""), "project_id")
	  (("verbose,v"), po::bool_switch(&verbose_flag), "verbose")
	  ;
    // clang-format on

    po::positional_options_description pos_desc;
    pos_desc.add(command_key.c_str(), 1);

    po::variables_map vm;

    try
    {
        po::store(po::command_line_parser(argc, argv).options(options).positional(pos_desc).run(), vm);
        po::notify(vm);
    }
    catch (po::error& e)
    {
        fprintf(stderr, "options error: %s\n", e.what());
        cout << options << '\n';
        return 1;
    }

    if (help_flag)
    {
        cout << options << '\n';
        return 0;
    }

    auto& config_file = vm[config_file_key].as<std::string>();
    const char* config_filename = nullptr;
    std::fstream file;
    if (strcmp(config_file.c_str(), default_config_file) != 0)
    {
        config_filename = config_file.c_str();
        if (!std::filesystem::is_regular_file(config_filename))
        {
            fprintf(stderr, "%s not a valid file [settings].\n", config_filename);
            return 1;
        }
        file.open(config_file, std::ios::in);
        if (!file.good())
        {
            fprintf(stderr, "%s bad file [settings].\n", config_filename);
            return 1;
        }
    }
    else
    {
        config_filename = default_config_file;
        file.open(default_config_file, std::ios::in);
    }
    auto& project_id_in_options = vm[project_id_key].as<std::string>();
    json::object config{};
    if (project_id_in_options.empty())
    {
        std::stringstream iss{};
        iss << file.rdbuf();
        json::error_code ec;
        auto parsed_config = json::parse(iss.str(), ec);
        if (ec)
        {
            fprintf(stderr, "settings file parse error: %s\n", ec.message().c_str());
            fprintf(stderr, "project_id not found in settings file and options.\n");
            return 1;
        }
        else
        {
            if (!parsed_config.is_object())
            {
                fprintf(stderr, "settings file contents not a valid json object\n");
                fprintf(stderr, "project_id not found in settings file and options.\n");
                return 1;
            }
            else
            {
                config = parsed_config.as_object();
                if (!config.contains("project_id"))
                {
                    fprintf(stderr, "project_id not found in settings file and options.\n");
                    return 1;
                }
                else
                {
                    project_id = config[project_id_key].as_string().c_str();
                }
            }
        }
    }
    else
    {
        project_id = vm[project_id_key].as<std::string>().c_str();
        config.emplace(project_id_key, project_id);
    }


    const std::array<std::string, 4> commands{"get", "create", "update", "list"};

    const bool command_found = std::find(commands.cbegin(), commands.cend(), vm[command_key].as<std::string>()) != commands.cend();

    if (vm[command_key].as<std::string>().empty() || !command_found)
    {
        fprintf(stderr, "command not found. use any of get, create, update, list\n");
        return 1;
    }
    const auto& command = vm[command_key].as<std::string>();

    curl_session sess(verbose_flag);

    const char* token = getenv(vm[token_var_key].as<std::string>().c_str());

    if (verbose_flag)
    {
        printf("command:\t%s\n", command.c_str());
        printf("token_var_key:\t%s\n", vm[token_var_key].as<std::string>().c_str());
    }

    if (!token || *token == 0)
    {
        fprintf(stderr, "token not found in env; exiting.\n");
        return 1;
    }

    sess.append_header(("Authorization: Bearer " + std::string(token)));
    labels_container labels;
    std::thread labels_thread(&fetch_project_labels, token, project_id, std::ref(labels), verbose_flag);


    bool ret;
    if (command == "create")
    {
        ret = issue::create(sess, labels, labels_thread);
    }
    else if (command == "list")
    {
        ret = issue::list(sess, vm[filter_key].as<const std::string>());
    }
    else if (command == "get")
    {
        ret = issue::get(sess, vm[issue_key].as<std::string>());
    }
    else
    {
        fprintf(stderr, "unhandled command:\t%s\n", command.c_str());
        ret = false;
    }

    if (labels_thread.joinable())
        labels_thread.join();
    config["labels"] = json::array(labels.begin(), labels.end());

    if (!ret)
        return 1;
    else
    {
        std::ofstream out(config_filename);
        out << config;
        return 0;
    }
}
