#ifndef GITLAB_API__LABELS_H
#define GITLAB_API__LABELS_H

#include <string>
#include <vector>

#include <boost/json.hpp>

#include "curl-session.h"
#include "utils.h"

using labels_container = std::vector<std::string>;

extern std::mutex mut;

void fetch_project_labels(const char* token, const char* project_id, labels_container& labels, bool verbose);

#endif //GITLAB_API__LABELS_H
