#ifndef GITLAB_API__ISSUE_H
#define GITLAB_API__ISSUE_H

#include <iostream>

#include <boost/json.hpp>

#include "curl-session.h"
#include "labels.h"
#include "utils.h"

class key_error : public std::invalid_argument
{
public:
    explicit key_error(const char* err):
        std::invalid_argument(err) {}
};

/*
 * project_id declared here; defined in gitlab-api.cpp
 */
extern const char* project_id;


class issue
{
public:
    issue() noexcept:
        m_obj(), print_desc(true)
    {
        m_obj["title"] = "";
        m_obj["description"] = "";
        m_obj["labels"] = json::array{};
        m_obj["assignee_id"] = 0;
        m_obj["issue_type"] = "issue";
    };

    explicit issue(json::object&& obj, bool print_desc_):
        m_obj(obj), print_desc(print_desc_)
    {
        assert(*obj.storage() == *m_obj.storage());

        for (const auto& k : keys)
        {
            if (!obj.contains(k))
            {
                char error_msg[50];
                sprintf(error_msg, "key %s not found.", k);
                throw key_error(error_msg);
            }
        }
    };

    explicit issue(json::object&& obj):
        issue(std::move(obj), false){};


    [[nodiscard]] bool is_saved() const
    {
        return m_obj.contains("iid");
    }

    [[nodiscard]] auto iid() const
    {
        return m_obj.at("iid").as_int64();
    }

    [[nodiscard]] const char* desc() const
    {
        return _str_val("description");
    };

    [[nodiscard]] const char* title() const
    {
        return _str_val("title");
    };

    [[nodiscard]] const char* state() const
    {
        return _str_val("state");
    };

    [[nodiscard]] std::string closed_by() const
    {
        const json::object& closedby = m_obj.at("closed_by").as_object();
        std::string ret{"user: "};
        ret += closedby.at("username").as_string().c_str();
        ret += "\tname:";
        ret += closedby.at("name").as_string().c_str();
        return ret;
    };

    [[nodiscard]] inline bool is_closed() const
    {
        return strcmp(state(), "closed") == 0;
    }

    [[nodiscard]] inline const json::array& labels() const
    {
        return m_obj.at("labels").as_array();
    }

    static void print_header(std::ostream& os);
    static void print_break(std::ostream& os);
    friend std::ostream& operator<<(std::ostream& os, const issue& is);

    static constexpr const char* keys[] = {"iid", "title", "state", "labels"};
    static constexpr std::pair<const char*, int> widths[std::size(keys)] = {{keys[0], 5}, {keys[1], 60}, {keys[2], 10}, {keys[3], 40}};
    static bool is_issue(json::object& obj);

    inline void set_title(std::string& s)
    {
        m_obj["title"] = s;
    }

    inline void set_desc(std::string& s)
    {
        m_obj["description"] = s;
    }

    [[nodiscard]] inline const json::object& object() const
    {
        return m_obj;
    }

    inline void push_label(std::string& s)
    {
        json::array& labels = m_obj.at("labels").as_array();
        if (std::find(labels.cbegin(), labels.cend(), s.c_str()) == labels.cend())
            labels.push_back(s.c_str());
    }

    void save(curl_session& sess);

    inline static void print_issue(std::ostream& os, const issue& iss)
    {
        std::array<const issue, 1> arr{iss};
        print_issues(os, arr);
    }

    template<Iterable V>
    inline static void print_issues(std::ostream& os, const V& v)
    {
        os.setf(std::ios::left, std::ios::adjustfield);
        issue::print_break(os);
        issue::print_header(os);
        issue::print_break(os);
        for (const issue& iss : v)
        {
            os << iss;
        }
        issue::print_break(os);
    };

    /**
     * the separate thread that's fetching labels is allowed to run while the
     * user is typing input, and is joined before labels are printed.
     */
    static bool create(curl_session& sess, labels_container& labels, std::thread& t);
    static bool list(curl_session& sess, const std::string& filters);
    static bool get(curl_session& sess, const std::string& iid);

private:
    json::object m_obj;
    inline static auto issues_endpoint = []() {
        if (!project_id)
        {
            fprintf(stderr, "[issues]: project_id not initialized.\n");
            return gitlab_project_url + "/issues";
        }
        return (gitlab_project_url + project_id) + "/issues/";
    };
    const char* _str_val(const char* key) const
    {
        return m_obj.at(key).as_string().c_str();
    }
    bool print_desc;
};

std::ostream& operator<<(std::ostream& os, const issue& iss);
static void print_field(std::ostream& os, uint wid, const char* c);

#endif //GITLAB_API__ISSUE_H
